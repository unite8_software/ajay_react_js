import React from "react";

class CmpntDidMount extends React.Component {
  constructor() {
    super();
    this.state = {
      data: null,
    };
    console.log("Constructor method");
  }

  componentDidMount() {
    this.setState({ data: "Ajay" });
    console.log("Component did mount method");
  }

  render() {
    console.log("Render Method");
    return null;
  }
}

export default CmpntDidMount;
