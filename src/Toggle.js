import React from 'react';

class Toggle extends React.Component {
    constructor() {
        super();
        this.state = {
            show: true
        }
    }
    render() {
        return (
            <div>
                {this.state.show ? <p>Hide and Show</p> : null }
                <button onClick={() => {this.setState({show: !this.state.show})}}>
                    {this.state.show ? "Hide": "Show"}
                </button>
            </div>
        )
    }
}

export default Toggle;