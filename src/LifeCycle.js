import React from "react";

class LifeCycle extends React.Component {
  constructor() {
    super();
    console.log("Constructor method");
  }

  componentDidMount() {
    console.log("Component did mount method");
  }

  render() {
    console.log("Render Method");
    return null;
  }
}

export default LifeCycle;
