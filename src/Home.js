import React from "react";

function Home(props) {
  const red = () => {
    alert("red function called");
  };

  return (
    <div>
      <h5 onClick={red}>{props.text}</h5>
    </div>
  );
}

export default Home;
