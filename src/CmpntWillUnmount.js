import React from "react";
import User from "./User";

class CmpntWillUnmount extends React.Component {
  
  constructor(){
    super();
    this.state = {toggle: true}
  }

  render() {
    return (
      <div>
       {this.state.toggle ? <User />: null }
       <button onClick={() => {this.setState({toggle: !this.state.toggle}) }}>Delete User</button>
      </div>
    );
  }
}

export default CmpntWillUnmount;
