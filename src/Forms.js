import React from "react";
const Forms = (props) => {
  const [count, setCount] = React.useState(100);

  React.useEffect(() => {
    console.log(count);
  }, [count === 105]);

  return (
    <div>
      <h1>Hooks in use effect component {count}</h1>
      <button onClick={() => setCount(count + 1)}>Increment</button>
      <br />
      <br />
    </div>
  );
};

export default Forms;
