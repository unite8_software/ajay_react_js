import React from "react";

class CmntDidUpdt extends React.Component {
  constructor() {
    super();
    this.state = {
      active: null,
      who: null,
    };
  }

  componentDidUpdate() {
    console.log("Componet did update");
    if (this.state.who == null) this.setState({ who: "ajay anand Srivastava" });
  }

  render() {
    return (
      <div>
        <h3>React ComponentDidUpdate {this.state.who}</h3>
        <button
          onClick={() => {
            this.setState({ active: "yes" });
          }}
        >
          Active
        </button>
      </div>
    );
  }
}

export default CmntDidUpdt;
