import React from "react";

class State extends React.Component {
  constructor() {
    super();
    this.state = {
      count: 1,
    };
  }
  incCount = () => {
      this.setState({
        count: this.state.count + 1
      })
  }

  render() {
    return (
      <div>
          <span>No of count: {this.state.count}</span>
        <button onClick={() => {this.incCount()}}> Count</button>
      </div>
    );
  }
}

export default State;
