import React from "react";

class User extends React.Component {

  componentWillUnmount() {
    alert('User has been deleted');
  }

  render() {
    return (
      <div>
       <ul>
         <li>First Name: Ajay</li>
         <li>Middle Name: Anand</li>
         <li>Last Name: Srivastava</li>
       </ul>
      </div>
    );
  }
}

export default User;
