import "./App.css";
import InputForms from './InputForms';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <InputForms />
      </header>
    </div>
  );
}

export default App;
