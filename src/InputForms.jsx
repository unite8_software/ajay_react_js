import React from "react";

class InputForms extends React.Component {
  constructor() {
    super();
    this.state = {
      user: "",
      password: "",
      userError: "",
      pswdError: "",
    };
  }

  valid() {
    if (this.state.user.length < 3) {
      this.setState({ userError: "Your name is too small" });
    }

    else if (this.state.user.includes("@")) {
      this.setState({ userError: "Invalid Name" });
    }

    if (this.state.password.length < 7) {
      this.setState({ pswdError: "Your password is too small" });
    }

    else if (!this.state.password.includes("@")) {
      this.setState({ pswdError: "Weak Password" });
    } 
    else {
      return true;
    }
  }

  formSubmit() {
    if (this.valid()) {
      this.setState({ userError: "", pswdError: "" });
      alert("Form has been submitted");
    }
  }

  render() {
    return (
      <div>
        <h1>Form Handing</h1>
        <input
          type="text"
          name="user"
          placeholder="Enter username"
          onChange={(e) => this.setState({ user: e.target.value })}
        />
        {this.state.userError ? (
          <h6 style={{ color: "orangered" }}>{this.state.userError}</h6>
        ) : (
          <h6> </h6>
        )}
        <input
          type="text"
          name="password"
          placeholder="Enter password"
          onChange={(e) => this.setState({ password: e.target.value })}
        />
        {this.state.userError ? (
          <h6 style={{ color: "orangered" }}>{this.state.pswdError}</h6>
        ) : (
          <h6> </h6>
        )}
        <button onClick={() => this.formSubmit()}>Submit</button>
      </div>
    );
  }
}

export default InputForms;
