import React from "react";

function Hooks() {
    const [count, setCount] = React.useState(100);

    return <div>
        <h1>Hooks in functional component {count}</h1>
        <button onClick={() => (setCount(count+1))}>Increment</button>

        <br />
        <br />
    </div>

}

export default Hooks;